//
//  ViewController.swift
//  testWkWebsite
//
//  Created by Dimitris Theodorou on 24/05/2018.
//  Copyright © 2018 Dimitris Theodorou. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, WKHTTPCookieStoreObserver, WKNavigationDelegate, UITextFieldDelegate  {
    //MARK: Properties
    @IBOutlet weak var reloadButton: UIButton!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var reloadText: UILabel!
    
    @IBOutlet weak var reload2Button: UIButton!
    @IBOutlet weak var go2Button: UIButton!
    @IBOutlet weak var url2TextField: UITextField!
    @IBOutlet weak var navStatus2: UILabel!
    
    @IBOutlet weak var containerView1: UIView!
    @IBOutlet weak var containerView2: UIView!
    
//    @IBOutlet weak var webView3: WKWebView!
//    @IBOutlet weak var webView2: WKWebView!
//    @IBOutlet weak var webView: WKWebView!
    var webView2: WKWebView!
    var cookieObs2: CookieChangeToConsole!
    var webView3: WKWebView!
    var cookieObs3: CookieChangeToConsole!
    var timesChanged = 1
    
    override func loadView() {
//        WKWebsiteDataStore.default().httpCookieStore.add(CookieChangeToConsole())
//        WKWebsiteDataStore.default().httpCookieStore.add(self)
//        cookieListener = CookieChangeToConsole()
//        let webConfiguration = WKWebViewConfiguration()
//        webConfiguration.websiteDataStore.httpCookieStore.add(self)
        super.loadView()
        
//        let defaultDataStore = WKWebsiteDataStore.default()
        let ppool = WKProcessPool()
        
        let webConfiguration2 = WKWebViewConfiguration()
        webConfiguration2.processPool = ppool
//        webConfiguration2.websiteDataStore.httpCookieStore.add(self)
//        cookieObs2 = CookieChangeToConsole("1")
//        webConfiguration2.websiteDataStore = defaultDataStore
//        webConfiguration2.websiteDataStore.httpCookieStore.add(cookieObs2)

        webView2 = WKWebView(
            frame: CGRect(x: 0, y: 0, width: containerView1.frame.width, height: containerView1.frame.height),
            configuration: webConfiguration2
        )
        containerView1.addSubview(webView2)
        
        let webConfiguration3 = WKWebViewConfiguration()
        webConfiguration3.processPool = ppool
//        cookieObs3 = CookieChangeToConsole("2")
//        webConfiguration3.websiteDataStore = webConfiguration2.websiteDataStore
//        webConfiguration3.websiteDataStore.httpCookieStore.add(cookieObs3)
        
//        print(webConfiguration2.websiteDataStore.httpCookieStore == webConfiguration3.websiteDataStore.httpCookieStore)
//        print(webConfiguration2.websiteDataStore.httpCookieStore === webConfiguration3.websiteDataStore.httpCookieStore)
        
        webView3 = WKWebView(
            frame: CGRect(x: 0, y: 0, width: containerView2.frame.width, height: containerView2.frame.height),
            configuration: webConfiguration3
        )
        containerView2.addSubview(webView3)
        
//        webView2.configuration = webConfiguration
//        webView3.configuration = webConfiguration

//        let frame = CGRect(origin: CGPoint.zero, size: CGSize(width:100, height: 100))
//        webView2 = WKWebView(frame: .zero, configuration: webConfiguration)
//        webView2.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        webView.configuration.websiteDataStore.httpCookieStore.add(cookieListener)

//        webView2.uiDelegate = self
        //view = webView
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

//        webView2.configuration.websiteDataStore.httpCookieStore.add(self)
        print(reloadButton, reloadText)
        print(webView2)
        urlTextField.delegate = self
        webView2.uiDelegate = self
        webView2.navigationDelegate = self
//        webView.configuration.websiteDataStore.httpCookieStore.add(self)
        //webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let initialUrl = "https://www.bloomingdales.com"
        
        urlTextField.text = initialUrl
        url2TextField.text = initialUrl
        _goToURL(webView2, initialUrl)
//        let myURL = URL(string: "https://www.bloomingdales.com")
//        let myRequest = URLRequest(url: myURL!)
//        webView2.load(myRequest)
        
//        let cookieStore = webView.configuration.websiteDataStore.httpCookieStore
//        printCookies(cookieStore)
//        cookieStore.add(CookieChangeToConsole())

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func _goToURL(_ webView: WKWebView, _ url: String) {
        let myURL = URL(string: url)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        urlTextField.text = textField.text
//    }
    
    //MARK: WKHTTPCookieStoreObserver
    func cookiesDidChange(in cookieStore: WKHTTPCookieStore) {
        timesChanged += 1
        print(timesChanged)
        //        printCookies(cookieStore)
    }
    
    //MARK: WKNavigationDelegate
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        if webView == webView2 {
            reloadText.text = "Started navigation"
        } else if webView == webView3 {
            navStatus2.text =   "Started navigation"
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if webView == webView2 {
            reloadText.text = "Finished navigation"
            urlTextField.text = webView.url!.absoluteString
        } else if webView == webView3 {
            navStatus2.text =   "Finished navigation"
            url2TextField.text = webView.url!.absoluteString
        }
    }
    
    //MARK: Actions
    @IBAction func reload(_ sender: UIButton) {
        webView2.reload()
    }
    

    @IBAction func reload2(_ sender: Any) {
        webView3.reload()
    }
    @IBAction func goToURL(_ sender: UIButton) {
        if urlTextField.text != "" {
            _goToURL(webView2, urlTextField.text!)
        }
    }
    
    @IBAction func goToURL2(_ sender: Any) {
        if url2TextField.text != "" {
            _goToURL(webView3, url2TextField.text!)
        }
    }
    
    
}

func printCookies(_ cookieStore: WKHTTPCookieStore) {
    cookieStore.getAllCookies() { (cookies) in
        for cookie in cookies {
            print(cookie)
        }
    }
}

class CookieChangeToConsole: NSObject, WKHTTPCookieStoreObserver {
    var timesChanged = 1
    var s: String
    
    init(_ s_: String){
        s = s_
        super.init()
    }
    
    func cookiesDidChange(in cookieStore: WKHTTPCookieStore) {
        timesChanged += 1
        print(s, timesChanged)
        //        printCookies(cookieStore)
    }
}
